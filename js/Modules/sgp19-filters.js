var swiperFlag = false,
filterSwiper;

function initSwiper(flag) {
    var settings = {
        loop: false,
        grabCursor: false,
        slidesPerView: 4,
        spaceBetween: 30,
        centeredSlides: true,
        autoHeight: true,
        initialSlide: 2,
        pagination: {
            el: '.swiper-pagination',
            type: 'fraction',
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 3,
                initialSlide: 1,
            },
            768: {
                slidesPerView: 2,
                initialSlide: 0,
            },
            500: {
                slidesPerView: 1,
                initialSlide: 0,
            }
        }
    };

    if (flag == false) {
        filterSwiper = new Swiper('.swiper-container', settings);
    } else {
        filterSwiper.update()
    }
}

function initDaterangePicker() {
    $('.daterangepicker').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        timePicker: false,
        locale: {
            format: 'M/DD/YYYY'
        },
        minDate: new Date(),
        maxYear: new Date().getFullYear() + 10,
    }, function (start, end, label) {
        // something
    });
}

function filters() {
    var d = new Date(),
    day = d.getDate(),
    month = d.getMonth() + 1,
    year = d.getFullYear();

    if (day < 10) day = '0' + day;
    if (month < 10) month = '0' + month;

    var today = year + '-' + month + '-' + day + ' 00:00:00',
    date = new Date(),
    tempDate,
    days,
    endDate,
    arr = [];

    $('.daterangepicker').on('apply.daterangepicker', function (ev, picker) {
        today = picker.startDate.format('YYYY-MM-DD 00:00:00');
        date = picker.startDate.format();
    });

    $('.filters-inputs__button .cta_button').click(function (e) {
        e.preventDefault()
        e.stopPropagation()

        var ageGroup = $('.filters-inputs__input .styled select').val();

        tempDate = new Date(date)
        days = parseInt($('.filters-inputs__input .number-input input').val(), 10);
        arr = [];

        Date.prototype.toInputFormat = function () {
            var yyyy = this.getFullYear().toString(),
            mm = (this.getMonth() + 1).toString(),
            dd = this.getDate().toString();
            return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]) + ' 00:00:00';
        };

        arr.push(today)

        for (var i = 1; i < days; i++) {
            tempDate.setDate(tempDate.getDate() + 1);
            endDate = tempDate.toInputFormat()
            arr.push(endDate)
        }

        $('.swiper-slide').each(function () {
            $(this).hide();
            $(this).removeClass('showed');
            $(this).removeClass('ski-actv');
        })

        // filter this
        var slidesArr = []
        var foundFirstActiveSlide = 0;

        arr.forEach(function (item) {
            $('.filters-list .swiper-slide').each(function (j, f) {
                var fDate, lDate, cDate, skiDays, days;

                fDate = $(f).attr('data-start-date');
                cDate = item;
                lDate = $(f).attr('data-end-date');

                days = parseInt($('.filters-inputs__input .number-input input').val(), 10);
                skiDays = parseInt($(f).attr('data-ski-ride-days'));

                if (Date.parse(cDate) <= Date.parse(lDate) && Date.parse(cDate) >= Date.parse(fDate) && $(f).attr('data-group') == ageGroup) {
                    if (days == skiDays) {
                        $(f).addClass('ski-actv')
                        if (foundFirstActiveSlide === 0) {
                            slidesArr.push('ski-actv')
                            foundFirstActiveSlide = 1;
                        } else {
                            slidesArr.push('showed')
                        }
                    } else {
                        slidesArr.push('showed')
                    }
                    $(f).addClass('showed').show();
                }
            })
        });

        swiperFlag = true
        initSwiper(swiperFlag);
        filterSwiper.slideTo(slidesArr.indexOf('ski-actv'), 400, false);
    });
}

function stepUpstepDownInput() {
    $('.input-custom-arr-minus').click(function (e) {
        e.preventDefault();

        var $button = $(this),
        oldValue = $button.parent().find("input").val(),
        newVal;

        if (oldValue > 1)
            newVal = parseFloat(oldValue) - 1;
        else
            newVal = 1;


        $button.parent().find("input").val(newVal);
    })

    $('.input-custom-arr-plus').click(function (e) {
        e.preventDefault();

        var $button = $(this),
        oldValue = $button.parent().find("input").val(),
        newVal;

        if (oldValue < 14)
            newVal = parseFloat(oldValue) + 1;
        else
            newVal = 14;

        $button.parent().find("input").val(newVal);
    })
}

function setInitialHeight() {
    var maxHeight = Math.max.apply(null, $(".swiper-slide").map(function () {
        return $(this).height();
    }).get()),

    maxHeightHeader = Math.max.apply(null, $(".filters-list__item__header").map(function () {
        return $(this).outerHeight();
    }).get()),

    maxHeightHeaderSec = Math.max.apply(null, $(".filters-list__item__subheader").map(function () {
        return $(this).outerHeight();
    }).get());


    $('.filters-list__item__header').css('min-height', maxHeightHeader);
    $('.filters-list__item__subheader').css('min-height', maxHeightHeaderSec);

    $('.swiper-slide').css('min-height', maxHeight)
    $('.filters-list__item').css('min-height', maxHeight)
}

$(function () {
    if ($('.filters-inputs').length) {
        initSwiper(swiperFlag);
        initDaterangePicker();
        filters();
        stepUpstepDownInput();
        setInitialHeight();
    }
})

window.onresize = function () {
    setTimeout(function () {
        setInitialHeight();
    }, 50)
}