$('.sgp19-header__menu .menu-column').each(function() {
	$(this).parent().parent().parent().addClass('menu-columns');
})

$('.sgp19-header__menu .hs-menu-depth-2').each(function() {
	if ($(this).next('.hs-menu-depth-2').length) {
		$(this).after('<li class="divider"></li>');
	}
})

$('.sgp19-header__menu li.hs-menu-depth-1').hover(function() {
	$(this).addClass('hovered');

	var container = $(this).find('.hs-menu-children-wrapper').first();
	var containerWidth = container.innerWidth();
	var containerPaddings = parseFloat(container.css('padding-left')) + parseFloat(container.css('padding-right'));

	var totalItemsWidth = 0;
	$(this).find('.hs-menu-children-wrapper').first().find('> li').each(function() {
		totalItemsWidth += $(this).innerWidth();
	});

	if ((containerWidth - containerPaddings) <= totalItemsWidth) {
		container.addClass('hs-menu-children-wrapper--expand')
	}

}, function() {
	$(this).removeClass('hovered');

	var container = $(this).find('.hs-menu-children-wrapper').first();
	container.removeClass('hs-menu-children-wrapper--expand')
});


var mobileMenu = $('.sgp19-header__menu').clone();
mobileMenu.find('.menu-columns').each(function() {
	var menuColumn = $(this);

	$(this).find('> li').each(function () {
		$(menuColumn).append($(this).find('li'));
		$(this).remove();
	});
});
mobileMenu.find('.menu-columns .hs-menu-children-wrapper').remove();

mobileMenu.slicknav({
	duplicate: false,
	duration: 400,
	removeClasses: true,
	prependTo: $('.sgp19-mobile-menu__nav'),
	label: '',
	init: function() {
		mobileMenu.append('<div class="slicknav-search"></div>');
		$('.slicknav-search').append($('.sgp19-search').clone());
		mobileMenu.append('<div class="slicknav-wheather-box"></div>');
		$('.slicknav-wheather-box').append($('.sgp19-weather-box').clone().addClass('cloned').removeClass('oryginal'));

	}
});

$('.slicknav-close a').on('click', function() {
	mobileMenu.slicknav('close');
});


$('.sgp19-mobile-menu__hamburger').on('click', function() {
	$(this).toggleClass('is-active');
	$(mobileMenu).slicknav('toggle');
});