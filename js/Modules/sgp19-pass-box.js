$(document).ready(function() {

	if ($('.pass-box').length > 0) {

		var $grid = $('.pass-box-wrapper');
		$grid.isotope({
			itemSelector: '.pass-box',
			percentPosition: true,
			masonry: {
				columnWidth: '.grid-sizer',
				gutter: '.gutter-sizer'
			}
		});

		$grid.addClass('pass-box-wrapper--initialized')

		$('.me-isotope-last-item').removeClass('me-isotope-last-item');
		var lastItem = $grid.find('> div').last();
		$(lastItem).addClass('me-isotope-last-item');
	}

});


$(window).load(function() {

	if ($('.me-pagination--js .me-pagination__navigation').length > 0) {
		paginationInit();
	}

	if ($('.me-pagination--js .me-pagination__load-more').length > 0) {
		paginationInit();
		loadMoreInit();
		loadMore();
	}
	
	if ($('.me-category-filter').length > 0) {
		categoryFilterInit();
	}
});


// for me-category-filter
function categoryFilterInit() {
	var filterId = 1;

	$('.me-category-filter-element').parents('.hs_cos_wrapper_type_widget_container').addClass('me-category-filter-target');
	// $('.me-category-filter-element').removeClass('me-category-filter-element').parents('.hs_cos_wrapper_type_module').addClass('me-category-filter-element');


	$('.me-category-filter-target').each(function() {
		$(this).addClass('me-category-filter-target-' + filterId);

		if ($('.me-pagination--js[data-pagination-id=' + filterId + ']').length == 0) {
			$(this).find('.me-category-filter-element').attr('data-page', '1');
		}

		filterId++;
	});


	filterId = 1;
	$('.me-category-filter').each(function() {
		$(this).attr('data-filter-id', filterId)
		$(this).find('.me-category-filter__categories-item').first().click();
		filterId++;
	});
}

$('.me-category-filter__categories-item').on('click', function() {
	$(this).parents('.me-category-filter').find('.me-category-filter__categories-item').removeClass('me-category-filter__categories-item--active');
	$(this).addClass('me-category-filter__categories-item--active');

	var filterId = $(this).parents('.me-category-filter').data('filter-id');
	var itemsPerPage = $('.me-pagination--js[data-pagination-id=' + filterId + ']').data('items-per-page');
	var activeCategory = $(this).attr('data-filter');


	if (activeCategory === '*' || typeof activeCategory === 'undefined') {
		activeCategory = '.all';
		var totalItems = $('.me-category-filter-target-' + filterId + ' .me-category-filter-element').length;
	} else {
		var totalItems = $('.me-category-filter-target-' + filterId + ' ' + activeCategory).length;
	}

	if ($('.me-pagination--js[data-pagination-id=' + filterId + ']').length > 0) {
		var maxPages = Math.ceil(totalItems / itemsPerPage);

		$('.me-category-filter-target-' + filterId + ' .me-category-filter-element' + activeCategory).each(function(i) {
			$(this).attr("data-page", Math.ceil((i + 1) / itemsPerPage));
		});

		generatePagination(filterId, 1, maxPages);
	} else {
		$('.me-category-filter-target-' + filterId + ' .me-category-filter-element').each(function(i) {
			$(this).attr("data-page", '1');
		});
	}

	var $grid = $('.me-category-filter-target-' + filterId);
	$grid.isotope({
		percentPosition: true,
		filter: function() {
			if ($(this).hasClass(activeCategory.replace('.', '')) && parseInt($(this).attr('data-page')) === 1) {
				return true;
			}
		}
	});


	if ($grid.data('isotope').filteredItems.length > 0) {
		var lastItem = $grid.data('isotope').filteredItems[$grid.data('isotope').filteredItems.length - 1].element;
		$('.me-isotope-last-item').removeClass('me-isotope-last-item');
		$(lastItem).addClass('me-isotope-last-item');
	}

	// scripts to display load more button
	if ($('.me-pagination--js[data-pagination-id=' + filterId + '] .me-pagination__load-more').length > 0) {
		var loadMoreItems = 0;
		$('.me-pagination-target-' + filterId + ' .me-pagination-element' + activeCategory).each(function(i) {
			if ($(this).attr('data-page') > 1 && loadMoreItems < itemsPerPage) {
				loadMoreItems++;
			}
		});

		if (loadMoreItems == 0) {
			$('.me-pagination--js[data-pagination-id=' + filterId + '] .me-pagination__load-more a').fadeOut();
		} else {
			$('.me-pagination--js[data-pagination-id=' + filterId + '] .me-pagination__load-more a').fadeIn();
		}
	}

	// fix for wow effect
	setTimeout(function() {
		$(window).scrollTop($(window).scrollTop() + 1);
		$(window).scrollTop($(window).scrollTop() - 1);
	}, 400);
});




// for me-pagination
function paginationInit() {
	var paginationId = 1;

	$('.me-pagination-element').parents('.hs_cos_wrapper_type_widget_container').addClass('me-pagination-target');
	// $('.me-pagination-element').removeClass('me-pagination-element').parents('.hs_cos_wrapper_type_module').addClass('me-pagination-element');

	$('.me-pagination--js').each(function() {
		$(this).attr('data-pagination-id', paginationId)
		paginationId++;
	});

	paginationId = 1;
	$('.me-pagination-target').each(function() {
		$(this).addClass('me-pagination-target-' + paginationId);
		paginationId++;
	});

	$('.me-pagination--js').each(function() {
		var paginationId = $(this).attr('data-pagination-id');
		var itemsPerPage = $(this).data('items-per-page');
		var totalItems = $('.me-pagination-target-' + paginationId + ' .me-pagination-element').length;
		var maxPages = Math.ceil(totalItems / itemsPerPage);

		$('.me-pagination-target-' + paginationId + ' .me-pagination-element').each(function(i) {
			$(this).attr("data-page", Math.ceil((i + 1) / itemsPerPage));
		});

		generatePagination(paginationId, 1, maxPages);

		var $grid = $('.me-pagination-target-' + paginationId);
		$grid.isotope({
			percentPosition: true,
			filter: '[data-page=1]'
		});

		if ($grid.data('isotope').filteredItems.length > 0) {
			var lastItem = $grid.data('isotope').filteredItems[$grid.data('isotope').filteredItems.length - 1].element;
			$('.me-isotope-last-item').removeClass('me-isotope-last-item');
			$(lastItem).addClass('me-isotope-last-item');
		}

	});
}

function changePage(paginationId, page) {
	var activeCategory = $('.me-category-filter[data-filter-id=' + paginationId + '] .me-category-filter__categories-item--active').data('filter');
	var itemsPerPage = $('.me-pagination--js[data-pagination-id=' + paginationId + ']').data('items-per-page');

	if (activeCategory == '*' || typeof activeCategory === 'undefined') {
		activeCategory = '.all';
		var totalItems = $('.me-pagination-target-' + paginationId + ' .me-pagination-element').length;
	} else {
		var totalItems = $('.me-pagination-target-' + paginationId + ' ' + activeCategory).length;
	}

	var maxPages = Math.ceil(totalItems / itemsPerPage);

	var $grid = $('.me-pagination-target-' + paginationId);
	$grid.isotope({
		percentPosition: true,
		filter: function() {
			if ($(this).hasClass(activeCategory.replace('.', '')) && parseInt($(this).attr('data-page')) === page) {
				return true;
			}
		}
	});

	if ($grid.data('isotope').filteredItems.length > 0) {
		var lastItem = $grid.data('isotope').filteredItems[$grid.data('isotope').filteredItems.length - 1].element;
		$('.me-isotope-last-item').removeClass('me-isotope-last-item');
		$(lastItem).addClass('me-isotope-last-item');
	}

	setTimeout(function() {
		generatePagination(paginationId, page, maxPages);
	}, 300);

	if ($('.me-category-filter[data-filter-id=' + paginationId +']').length > 0) {
		$('html, body').animate({
			scrollTop: $('.me-category-filter[data-filter-id=' + paginationId +']').offset().top - 130
		}, 900);
	} else {
		$('html, body').animate({
			scrollTop: $('.me-pagination-target-' + paginationId).offset().top - 130
		}, 900);
	}
}


function generatePagination(paginationId, currentPage, totalPages) {
	var paginationContainer = $('.me-pagination--js[data-pagination-id=' + paginationId + '] .me-pagination__navigation');
	var currentPage = currentPage;
	var page = currentPage;
	var total_pages = totalPages;
	var more_pages = total_pages - currentPage;
	var paginationHtml = '';
	paginationHtml = $('<ul></ul>');
	if (currentPage > 1) {
		paginationHtml.append('<li class="me-pagination__page"><a href="javascript:changePage(' + paginationId + ', ' + (page - 1) + ')"><i class="fas fa-chevron-left"></i></a></li>');
	}

	if (more_pages == 0) {
		if (currentPage - 4 == 1)
			paginationHtml.append('<li class="me-pagination__page"><a href="javascript:changePage(' + paginationId + ', ' + (page - 4) + ')">' + (page - 4) + '</a></li>');
		if (currentPage - 4 > 1)
			paginationHtml.append('<li class="me-pagination__page"><a href="javascript:changePage(' + paginationId + ', ' + (page - 4) + ')">' + (page - 4) + '</a></li>');
	}
	if (more_pages <= 1) {
		if (currentPage - 3 == 1)
			paginationHtml.append('<li class="me-pagination__page"><a href="javascript:changePage(' + paginationId + ', ' + (page - 3) + ')">' + (page - 3) + '</a></li>');
		if (currentPage - 3 > 1)
			paginationHtml.append('<li class="me-pagination__page"><a href="javascript:changePage(' + paginationId + ', ' + (page - 3) + ')">' + (page - 3) + '</a></li>');
	}

	if (currentPage - 2 == 1)
		paginationHtml.append('<li class="me-pagination__page"><a href="javascript:changePage(' + paginationId + ', ' + (page - 2) + ')">' + (page - 2) + '</a></li>');
	if (currentPage - 2 > 1)
		paginationHtml.append('<li class="me-pagination__page"><a href="javascript:changePage(' + paginationId + ', ' + (page - 2) + ')">' + (page - 2) + '</a></li>');
	if (currentPage - 1 == 1)
		paginationHtml.append('<li class="me-pagination__page"><a href="javascript:changePage(' + paginationId + ', ' + (page - 1) + ')">' + (page - 1) + '</a></li>');
	if (currentPage - 1 > 1)
		paginationHtml.append('<li class="me-pagination__page"><a href="javascript:changePage(' + paginationId + ', ' + (page - 1) + ')">' + (page - 1) + '</a></li>');

	paginationHtml.append('<li class="me-pagination__page me-pagination__page--active"><a href="javascript:changePage(' + paginationId + ', ' + (page) + ')">' + (page) + '</a></li>');

	if (currentPage + 1 <= total_pages)
		paginationHtml.append('<li class="me-pagination__page"><a href="javascript:changePage(' + paginationId + ', ' + (page + 1) + ')">' + (page + 1) + '</a></li>');
	if (currentPage + 2 <= total_pages)
		paginationHtml.append('<li class="me-pagination__page"><a href="javascript:changePage(' + paginationId + ', ' + (page + 2) + ')">' + (page + 2) + '</a></li>');

	if (currentPage <= 2) {
		if (currentPage + 3 <= total_pages)
			paginationHtml.append('<li class="me-pagination__page"><a href="javascript:changePage(' + paginationId + ', ' + (page + 3) + ')">' + (page + 3) + '</a></li>');
	}
	if (currentPage <= 1) {
		if (currentPage + 4 <= total_pages)
			paginationHtml.append('<li class="me-pagination__page"><a href="javascript:changePage(' + paginationId + ', ' + (page + 4) + ')">' + (page + 4) + '</a></li>');
	}

	if (currentPage < total_pages) {
		paginationHtml.append('<li class="me-pagination__page"><a <a href="javascript:changePage(' + paginationId + ', ' + (page + 1) + ')"><i class="fas fa-chevron-right"></i></a></li>');
	}

	if (total_pages > 1) {
		paginationContainer.html(paginationHtml);
	} else {
		paginationContainer.html('');
	}
}


function loadMoreInit() {

	$('.me-pagination--js').each(function() {
		var paginationId = $(this).attr('data-pagination-id');
		var itemsPerPage = $(this).data('items-per-page');
		var activeCategory = $('.me-category-filter[data-filter-id=' + paginationId + '] .me-category-filter__categories-item--active').data('filter');

		if (activeCategory == '*' || typeof activeCategory === 'undefined') {
			activeCategory = '.all';
		}

		var loadMoreItems = 0;
		$('.me-pagination-target-' + paginationId + ' .me-pagination-element' + activeCategory).each(function(i) {
			if ($(this).attr('data-page') > 1 && loadMoreItems < itemsPerPage) {
				loadMoreItems++;
			}
		});

		if (loadMoreItems > 0) {
			$(this).find('.me-pagination__load-more a').fadeIn();
		}
	});
}



function loadMore() {

	$('.me-pagination__load-more a').on('click', function(e) {
		e.preventDefault();

		var paginationId = $(this).parents('.me-pagination--js').data('pagination-id');
		var itemsPerPage = $('.me-pagination--js[data-pagination-id=' + paginationId + ']').data('items-per-page');
		var loadMoreNum = $(this).data('items-to-load');
		
		var activeCategory = $('.me-category-filter[data-filter-id=' + paginationId + '] .me-category-filter__categories-item--active').data('filter');

		if (activeCategory == '*' || typeof activeCategory === 'undefined') {
			activeCategory = '.all';
		}


		var loadMoreItems = 0;
		$('.me-pagination-target-' + paginationId + ' .me-pagination-element' + activeCategory).each(function(i) {
			if ($(this).attr('data-page') > 1 && loadMoreItems < loadMoreNum) {
				$(this).attr('data-page', '1');
				loadMoreItems++;
			}
		});


		var $grid = $('.me-pagination-target-' + paginationId);
		$grid.isotope({
			percentPosition: true,
			filter: function() {
				if ($(this).hasClass(activeCategory.replace('.', '')) && parseInt($(this).attr('data-page')) === 1) {
					return true;
				}
			}
		});

		if ($grid.data('isotope').filteredItems.length > 0) {
			var lastItem = $grid.data('isotope').filteredItems[$grid.data('isotope').filteredItems.length - 1].element;
			$('.me-isotope-last-item').removeClass('me-isotope-last-item');
			$(lastItem).addClass('me-isotope-last-item');
		}

		loadMoreItems = 0;
		$('.me-pagination-target-' + paginationId + ' .me-pagination-element' + activeCategory).each(function(i) {
			if ($(this).attr('data-page') > 1 && loadMoreItems < loadMoreNum) {
				loadMoreItems++;
			}
		});

		if (loadMoreItems == 0) {
			$(this).fadeOut();
		}

		setTimeout(function() {
			$(window).scrollTop($(window).scrollTop() + 1);
			$(window).scrollTop($(window).scrollTop() - 1);
		}, 400);
	});
}